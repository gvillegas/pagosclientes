/**
 * Created by pecesillo on 16-08-2017.
 */

function delthisdiv(t) {
    $(t).remove();
}

function agregaPago() {
    var formas_pagos = '';
    var formas_pago_objects = '';

         $.getJSON('/jsonResponse?formas_pago', function(data) {
            formas_pago_objects = JSON.parse(data);
            $.each(formas_pago_objects, function(key, value) {
                formas_pagos = formas_pagos + '<option value="'+key+'">'+ value.nombre +'</option>';

            });

            var html = '<div id="pagos"><div class="ln_solid"></div><h2>Pagos ' +
                '<small>Ingresar pago correspondiente a boleta pagada</small></h2><div class="ln_solid"></div>' +
                '<div class="col-md-2 col-sm-2 col-xs-2 form-group ">' +
                '<label for="forma_pago">Forma de pago</label>' +
                '<select name="forma_pago" id="forma_pago" class="form-control">' +
                formas_pagos +
                '</select>' +
                '</div><div class="col-md-2 col-sm-2 col-xs-2 form-group">' +
                '<label for="color">Fecha de pago *</label><input type="text" class="form-control calendar_single" id="fecha_pago" name="fecha_pago" placeholder="">' +
                '</div> <div class="col-md-2 col-sm-6 col-xs-12 form-group "><label for="monto_pago">Monto</label> ' +
                '<input type="number" id="monto_pago" name="monto_pago" class="form-control">' +
                ' </div> <div class="col-md-4 col-sm-6 col-xs-12 form-group"><label for="comantario">Comentario</label> <input type="text" name="comentario_pago" class="form-control" id="comentario_pago"></div>';
            $('#pagos-div').append(html);

                $(".calendar_single").daterangepicker({
                autoApply: true,
                singleDatePicker: true,
                timePicker24Hour: true,
                locale: {
                    format: 'DD/MM/YYYY'
                },
            });
         });

}

$('#cliente_nombre').autocomplete({
    deferRequestBy: 60,
    paramName: 'razon_social',
    dataType: 'json',
    serviceUrl: '/jsonResponse',
    onSelect: function (suggestion) {
        $('#cliente_rut').val(suggestion.data);
    }
});


$(".calendar_single").daterangepicker({
            autoApply: true,
            singleDatePicker: true,
            timePicker24Hour: true,
            locale: {
                format: 'DD/MM/YYYY'
            },
        });


function montoNeto(monto) {
    var montototal = 0;
    if ($('#ckRetenido').is(':checked')) {
            montototal = (monto * 0.9).toFixed(0);
    }else {
        montototal = monto;
    }
    $("#monto_total").val(montototal);
}

function checkeaMonto() {
    var monto_neto = $("#monto_neto").val();
    if ($('#ckRetenido').is(':checked')) {
       var montototal = (monto_neto * 0.9).toFixed(0);
       $("#monto_total").val(montototal);
    }else{
        $("#monto_total").val(monto_neto);
    }

}

function checkeaPago() {
    if ($('#ckPagado').is(':checked')) {
       agregaPago();
    }else{
        delthisdiv('#pagos');
    }

}

