import datetime

from django.contrib import messages
from django.core import serializers
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth.views import login
from django.views.decorators.csrf import csrf_exempt
from django_tables2 import RequestConfig
import ClientesPagos.mensajes as mensajes
from ClientesPagos.models import Boleta, User
from ClientesPagos.tables import BoletasTable
from ClientesPagos.forms import *
import json

# Create your views here.
from django.urls import reverse_lazy


def custom_login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse_lazy('index'))
    else:
        return login(request, template_name='login.html')


@login_required
def index(request):
    currentUser = User.objects.get(pk=request.user.id)
    return render(request, 'index.html')


@login_required
def listaBoletas(request):
    boletas = BoletasTable(Boleta.objects.filter(empresa_id=request.user.empresa_id).order_by('-folio'))
    RequestConfig(request, paginate={'per_page': 15}).configure(boletas)
    return render(request, 'listaBoletas.html', {'boletas': boletas})

@login_required
def boletaVerEditar(request):
    pass

@csrf_exempt
@login_required
def EmitirBoleta(request):
    if request.user.has_perm('ClientesPagos.add_boleta'):
        if request.method == 'POST':  # SI SOLO ENVIA EL FORMULARIO PARA AGREGAR
            for x in request.POST:
                print(request.POST[x])

            # VARIABLES DE LA BOLETA
            fecha_emision = request.POST['fecha_emision']
            fecha_emision = fecha_emision.split('/')
            fecha_emision = fecha_emision[2] + '-' + fecha_emision[1] + '-' + fecha_emision[0]
            cliente_rut = request.POST['cliente_rut']
            comentario = request.POST['comentario']
            folio = request.POST['folio']
            monto_neto = request.POST['monto_neto']
            if int(monto_neto) <= 0:
                messages.warning(request,mensajes.BOLETA_MONTO_VACIO)
                return render(request, 'emitirBoleta.html')

            ckPagado = request.POST['ckPagado']
            ckRetenido = request.POST['ckRetenido']
            is_retenido = False
            if ckRetenido == 'on':
                is_retenido = True
                monto_total = round((int(monto_neto) * 0.9), 0)
            else:
                monto_total = monto_neto

            # VARIABLES DEL PAGO
            forma_pago = request.POST['forma_pago']
            fecha_pago = request.POST['fecha_pago']
            monto_pago = request.POST['monto_pago']
            comentario_pago = request.POST['comentario_pago']

            pagado = False
            if ckPagado == 'on' and int(monto_total) == monto_pago:
                pagado = True

            nuevaBoleta = Boleta.objects.create(fecha_emision= fecha_emision,
                                                folio= folio,
                                                monto_neto = monto_neto,
                                                cliente_id = Cliente.objects.filter(rut__exact=cliente_rut)[0],
                                                empresa_id = request.user.empresa_id,
                                                pagado = pagado,
                                                comentario = comentario,
                                                impuesto_id = Impuesto.objects.all()[0],
                                                is_retenido = is_retenido,
                                                monto_total = monto_total
                                                )
            nuevaBoleta.save()
            proximo_folio = int(nuevaBoleta.folio) + 1
            messages.success(request, mensajes.BOLETA_EMISION_CORRECTA)
            return render(request, 'emitirBoleta.html', {'proximo_folio': proximo_folio})
        else:  # SI SOLO CARGA LA PAGINA
            tipo_pagos = TipoPago.objects.all()
            forma_pagos = FormaPago.objects.filter(empresa_id=request.user.empresa_id)
            ultima_boleta = Boleta.objects.filter(empresa_id=request.user.empresa_id).order_by('-folio')[0]
            proximo_folio = int(ultima_boleta.folio) + 1

            return render(request, 'emitirBoleta.html', {'proximo_folio': proximo_folio, 'forma_pagos':forma_pagos, 'tipo_pagos':tipo_pagos})
    else:
        return HttpResponseRedirect(reverse_lazy('index'))

@login_required
def jsonResponsesAutocomplete(request):
    if request.method == "GET":
        if "razon_social" in request.GET:
            clientes = Cliente.objects.filter(empresa_id=request.user.empresa_id)
            mapa_cliente = []
            for cliente in clientes:
                mapa_cliente.append({'value': cliente.razon_social, 'data': cliente.rut})

            suggestions = {"query": "Unit", "suggestions": mapa_cliente}
            respuesta_json = json.dumps(suggestions)
            print(respuesta_json)
            return JsonResponse(respuesta_json, safe=False)
        elif "formas_pago" in request.GET:
            forma_pagos = FormaPago.objects.filter(empresa_id=request.user.empresa_id)
            objetos = []
            for forma_pago in forma_pagos:
                objetos.append({'id':forma_pago.id, 'nombre':forma_pago.forma_pago})

            respuesta_json = json.dumps(objetos)
            return JsonResponse(respuesta_json, safe=False)
        elif "tipos_pago" in request.GET:
            tipo_pagos = TipoPago.objects.all()
            objetos = []
            for tipo_pago in tipo_pagos:
                objetos.append({'id':tipo_pago.id, 'nombre':tipo_pago.tipo_pago})
            respuesta_json = json.dumps(objetos)
            return JsonResponse(respuesta_json, safe=False)
        else:
            # OTRAS OPCIONES DE AUTOCOMPLETES FUTUROS
            return HttpResponseRedirect(reverse_lazy('index'))
    else:
        return HttpResponseRedirect(reverse_lazy('index'))