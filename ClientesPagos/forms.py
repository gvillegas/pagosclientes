from django import forms
from django.forms import ModelForm, inlineformset_factory
from django.contrib.auth.forms import UserCreationForm
from ClientesPagos.models import *


class BoletaForm(forms.ModelForm):

    class Meta:

        model = Boleta
        fields = ['folio', 'fecha_emision', 'impuesto_id', 'cliente_id', 'pagado', 'monto_neto', 'monto_total',
                  'comentario', 'is_retenido']