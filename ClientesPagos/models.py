from django.contrib.auth.validators import UnicodeUsernameValidator, ASCIIUsernameValidator
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser

# Create your models here.
from django.utils import six


class Empresa(models.Model):
    CUENTA_CORRIENTE = 'CUENTA_CORRIENTE'
    CUENTA_AHORRO = 'CUENTA_AHORRO'
    CUENTA_VISTA = 'CUENTA_VISTA'
    CUENTA_RUT = 'CUENTA_RUT'
    image = models.FileField(upload_to='static/uploads/')
    abreviado = models.CharField(max_length=5)
    nombre = models.CharField(max_length=30)
    rut = models.IntegerField()
    fono = models.CharField(max_length=12)
    razon_social = models.CharField(max_length=30, null=True)
    direccion = models.CharField(max_length=40, null=True)
    email = models.CharField(max_length=25)
    cuenta_bancaria = models.CharField(max_length=30)
    banco = models.CharField(max_length=30)
    TIPO_CUENTA_BANCARIA = (
        (CUENTA_CORRIENTE, 'Cuenta corriente'),
        (CUENTA_AHORRO, 'Cuenta de ahorro'),
        (CUENTA_VISTA, 'Cuenta vista'),
        (CUENTA_RUT, 'Cuenta RUT')
                            )
    tipo_cuenta_bancaria = models.CharField(max_length=20, choices=TIPO_CUENTA_BANCARIA)
    rubro = models.CharField(max_length=25)

    def __str__(self):
        return self.razon_social


class User(AbstractUser):
    username_validator = UnicodeUsernameValidator() if six.PY3 else ASCIIUsernameValidator()
    empresa_id = models.ForeignKey(Empresa, null=True)
    email = models.EmailField(_('email address'), unique=True)
    rut = models.IntegerField(null=True)
    username = models.CharField(
        _('username'),
        max_length=150,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that email already exists."),
        },
    )
    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.email


class Impuesto(models.Model):
    # el valor del impuesto es en notacion decimal ej iva = 19
    impuesto_nombre = models.CharField(max_length=20)
    impuesto_valor = models.IntegerField()

    def __str__(self):
        return self.impuesto_nombre


class Cliente(models.Model):
    empresa_id = models.ForeignKey(Empresa)
    rut = models.IntegerField()
    razon_social = models.CharField(max_length=30, null=True)
    email = models.CharField(max_length=30, null=True)
    fono = models.CharField(max_length=15, null=True)
    direccion = models.CharField(max_length=40)
    nombre_contacto = models.CharField(max_length=20)
    rubro = models.CharField(max_length=25)
    comentario = models.CharField(max_length=200)

    def __str__(self):
        return self.razon_social

    class Meta:
        unique_together = ('empresa_id', 'rut')


class TipoPago(models.Model):
    #  a 30 dias, etc... valor en dias
    tipo_pago = models.CharField(max_length=30)
    dias = models.IntegerField()

    def __str__(self):
        return self.tipo_pago


class Factura(models.Model):
    folio = models.IntegerField(unique=True)
    fecha_emision = models.DateField()
    fecha_pago = models.DateField(null=True)
    impuesto_id = models.ForeignKey(Impuesto)
    cliente_id = models.ForeignKey(Cliente)
    empresa_id = models.ForeignKey(Empresa)
    tipo_pago_id = models.ForeignKey(TipoPago)
    pagado = models.BooleanField()
    monto_neto = models.IntegerField()
    monto_total = models.IntegerField()
    comentario = models.CharField(max_length=200)

    def __str__(self):
        return self.folio


class FormaPago(models.Model):
    #  transferencia, deposito, efectivo
    forma_pago = models.CharField(max_length=30)
    empresa_id = models.ForeignKey(Empresa)

    def __str__(self):
        return self.forma_pago


class Boleta(models.Model):
    folio = models.IntegerField()
    fecha_emision = models.DateField()
    fecha_pago = models.DateField(null=True)
    impuesto_id = models.ForeignKey(Impuesto)
    cliente_id = models.ForeignKey(Cliente)
    empresa_id = models.ForeignKey(Empresa)
    pagado = models.BooleanField()
    monto_neto = models.IntegerField()
    monto_total = models.IntegerField()
    comentario = models.CharField(max_length=200)
    is_retenido = models.BooleanField()

    def __str__(self):
        return self.cliente_id.razon_social + " - " + str(self.folio)

    class Meta:
        unique_together = ('empresa_id', 'folio')


class Pago(models.Model):
    factura_id = models.ForeignKey(Factura, null=True)
    boleta_id = models.ForeignKey(Boleta, null=True)
    fecha_pago = models.DateField()
    forma_pago_id = models.ForeignKey(FormaPago)
    monto = models.IntegerField()
    comentario = models.CharField(max_length=25)

    def __str__(self):
        return self.id


















