from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Empresa)
admin.site.register(User)
admin.site.register(Impuesto)
admin.site.register(Cliente)
admin.site.register(TipoPago)
admin.site.register(Factura)
admin.site.register(FormaPago)
admin.site.register(Boleta)
admin.site.register(Pago)



