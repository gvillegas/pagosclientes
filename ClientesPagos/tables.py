from django_tables2 import A

from ClientesPagos.models import Boleta
from table.columns import Column
import django_tables2 as tables


class BoletasTable(tables.Table):
    folio = tables.Column(verbose_name='Folio')
    fecha_emision = tables.Column(verbose_name='Fecha de emision')
    cliente_id = tables.Column(verbose_name='Cliente')
    monto_neto = tables.Column(verbose_name='Monto neto')
    monto_total = tables.Column(verbose_name='Monto total')
    pagado = tables.BooleanColumn(verbose_name='Pagado?', yesno='SI,NO')
    is_retenido = tables.BooleanColumn(verbose_name='Impuesto retenido?', yesno='SI,NO')
    acciones = tables.TemplateColumn(template_code='<button class="btn btn-success btn-xs" onclick="window.location.href=\'/page2\'">Ver / Editar</button>'
                                                 '<button class="btn btn-dark btn-xs" onclick="window.location.href=\'/page2\'">Descargar</button>'
                                                 '<button {% if record.pagado %} disabled {% endif %} class="btn btn-dark btn-xs" onclick="window.location.href=\'/page2\'">Nuevo pago</button>'
                                                 '<button form="{{ record.id }}-form" type="button" '
                                                 'onclick="confirmarDel(this.form)" '
                                                 'class="btn btn-danger btn-xs">Anular</button>',
                                   template_name='Acciones')

    class Meta:
        model = Boleta
        fields = ('folio','fecha_emision','cliente_id','monto_neto','monto_total', 'pagado', 'is_retenido')
        attrs = {'class':'table table-striped table-bordered bulk_action dataTable no-footer', 'aling': 'center'}